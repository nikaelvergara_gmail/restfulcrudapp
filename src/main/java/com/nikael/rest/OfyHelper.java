package com.nikael.rest;

import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletContextListener;
import javax.servlet.ServletContextEvent;

public class OfyHelper implements ServletContextListener {
	public static void register() {
		ObjectifyService.register(Customer.class);
	}

	public void contextInitialized(ServletContextEvent event) {
		// it is a good practice to register objectify objects here
		register();
	}

	public void contextDestroyed(ServletContextEvent event) {
		// App Engine does not currently invoke this method.
	}
}
