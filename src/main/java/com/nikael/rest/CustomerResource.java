package com.nikael.rest;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.googlecode.objectify.ObjectifyService;

@Path("/customer")
public class CustomerResource {

	@GET
	@Path("/")
	@Produces("application/json")
	public List getAllCustomers() {
		List<Customer> customers = ObjectifyService.ofy().load().type(Customer.class).list();
		return customers;
	}

	@GET
	@Path("/{name}")
	@Produces("application/json")
	public Customer getCustomer(@PathParam("name") String name) {
		Customer customer = ObjectifyService.ofy().load().type(Customer.class).id(name).now();
		return customer;
	}

	@POST
	@Path("/")
	@Produces("application/json")
	public Response addCustomer(Customer customer) {
		ObjectifyService.ofy().save().entity(customer).now();
		return Response.ok().build();
	}

	@PUT
	@Path("/{name}")
	@Produces("application/json")
	public Response updateCustomer(@PathParam("name") String name, Customer customer) {
		Customer fetchCustomer = ObjectifyService.ofy().load().type(Customer.class).id(name).now();
		if (fetchCustomer == null) {
			return Response.status(404).build();
		} else {
			ObjectifyService.ofy().save().entity(customer).now();
			return Response.ok().build();
		}
	}

	@DELETE
	@Path("/{name}")
	@Produces("application/json")
	public Response deleteCustomer(@PathParam("name") String name) {
		Customer customer = ObjectifyService.ofy().load().type(Customer.class).id(name).now();
		if (customer == null) {
			return Response.status(404).build();
		} else {
			ObjectifyService.ofy().delete().entity(customer);
			return Response.ok().build();
		}
	}
}