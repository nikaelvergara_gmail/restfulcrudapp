require 'net/http'

# implementation should probably be place on a seperate file
base_url = ""
uri = URI.parse("http://localhost:8080")
http = Net::HTTP.new(uri.host, uri.port)

Given(/^that the application was successfully deployed$/) do
    request = Net::HTTP::Get.new("/")
    response = http.request(request)
    assert_equal "200", response.code
end

When(/^I create a new Customer using the following information$/) do |customers|
  customers_map = customers.hashes
  customers_map.each do  | customer |
    request = Net::HTTP::Post.new("/api/v1/customer")
    request['Content-Type'] = 'application/json'
    request.body = customer.to_json
    puts customer.to_json
    response = http.request(request)
  end
end

Then(/^there should be (.*) new customers$/) do |count|
    request = Net::HTTP::Get.new("/api/v1/customer")
    response = http.request(request)
    body = JSON.parse(response.body)
    assert_equal count.to_i, body.length
end

When(/^I update a customer named (.*) with the following information$/) do |name, customers|
    customers_map = customers.hashes
    customers_map.each do  | customer |
        request = Net::HTTP::Put.new("/api/v1/customer/#{name}")
        request.set_form_data(customer)
        request['Content-Type'] = 'application/json'
        @last_response = http.request(request)
    end
end

Then(/^I should receive a (\d+) status code result$/) do |code|
  # pending # express the regexp above with the code you wish you had
end

When(/^I delete a customer with named (.*)$/) do |name|
    request = Net::HTTP::Delete.new("/api/v1/customer/#{name}")
    response = http.request(request)
end