Feature: Basic REST test for restfulcrudapp
  To ensure that the rest server is working correctly
  As an unauthenticated user
  I should be able to make request externally

  Background:
    Given that the application was successfully deployed

  Scenario: Create new Customer
    When I create a new Customer using the following information
      | name | address    | telephoneNumber |
      | Foo  | 111 Foo St | 112233          |
      | Max  | 45 Max St  | 556554          |
      | Joe  | 191 Joe St | 458844          |
    Then there should be 3 new customers

  Scenario: Update a Customer with present and missing names
    When I update a customer named Joe with the following information
      | name | address    | telephoneNumber |
      | Joe  | 191 Joe St | 458844          |
    Then I should receive a 200 status code result
    When I update a customer named Missing with the following information
      | name     | address      | telephoneNumber |
      | Missing  | 1212 Gone St | 4678674         |
    Then I should receive a 404 status code result

  Scenario: Delete a Customer with present and missing names
    When I delete a customer with named Joe
    Then I should receive a 200 status code result
    When I delete a customer with named Missing
    Then I should receive a 404 status code result